# Copyright 2012-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require fs-uae-base [ branch=devel ]

MYOPTIONS="
    lua [[ description = [ Enable scripting support using LUA ] ]]
"

DEPENDENCIES="
    build:
        x11-libs/libXi [[ note = [ only needed for X11/extensions/XInput2.h ] ]]
    build+run:
        app-arch/zip
        dev-libs/glib:2[>=2.32]
        dev-libs/libfs-capsimage[>=4.2]
        media-libs/freetype:2
        media-libs/libpng
        media-libs/openal
        media-libs/SDL:2
        x11-dri/glu
        x11-dri/mesa
        x11-libs/libX11
        lua? ( dev-lang/lua )
    suggestion:
        app-emulation/fs-uae-arcade [[ description = [ Media center PC GUI (if you want to run fs-uae on your TV) ] ]]
        app-emulation/fs-uae-launcher [[ description = [ GUI (PyQt4) launcher for fs-uae ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # Avoid osmesa being pulled in by pkg-config
    GLEW_LIBS="-lGLEW"
    --datarootdir=/usr/share/fs-uae
    --enable-jit
    --enable-jit-fpu
    --with-glew
    --without-glad
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    lua
)

DEFAULT_SRC_COMPILE_PARAMS=( AR=/usr/$(exhost --target)/bin/$(exhost --tool-prefix)ar )

